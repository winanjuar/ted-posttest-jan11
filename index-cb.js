const express = require("express");
const BookController = require("./controllers/book");
const UserController = require("./controllers/user");
const TodoController = require("./controllers/todo");
const bcrypt = require("bcrypt");

const app = express();
const port = process.env.PORT || 3000;

const { Book, User, Todo } = require("./models");

app.use(express.json());

app.get("/", (req, res) => {
  res.status(200).json({
    status: true,
    message: "Tugas TED Express Sequelize using Callback",
  });
});

app.get("/books", (req, res) => {
  BookController.getAllBook((error, books) => {
    if (error) {
      res.json(error);
    }

    res.status(200).json({
      status: true,
      message: "All Books retrieved",
      data: books,
    });
  });
});

app.get("/books/:id", (req, res) => {
  const { id } = req.params;
  BookController.getBook(id, (error, book) => {
    if (error) {
      res.json(error);
    }

    res.status(200).json({
      status: true,
      message: `Book with id ${id} retrieved`,
      data: book,
    });
  });
});

app.post("/books", (req, res) => {
  const book = {
    title: req.body.title,
    author: req.body.author,
  };

  BookController.createBook(book, (error, bookInserted) => {
    if (error) {
      res.json(error);
    }

    res.status(201).json({
      status: true,
      message: "Books created!",
      data: bookInserted,
    });
  });
});

app.put("/books/:id", (req, res) => {
  const { id } = req.params;
  BookController.getBook(id, (error, book) => {
    if (error) {
      res.json(error);
    }

    const newBook = {
      title: req.body.title || book.title,
      author: req.body.author || book.author,
    };

    BookController.updateBook(id, newBook, (error, updatedBook) => {
      if (error) {
        res.json(error);
      }

      res.status(200).json({
        status: true,
        message: `Book with id ${id} updated!`,
      });
    });
  });
});

app.delete("/books/:id", (req, res) => {
  const { id } = req.params;
  BookController.deleteBook(id, (error, book) => {
    if (error) {
      res.json(error);
    }

    res.status(201).json({
      status: true,
      message: `Books with id ${id} deleted!`,
    });
  });
});

app.get("/users", (req, res) => {
  UserController.getAllUser((error, users) => {
    if (error) {
      res.json(error);
    }

    if (users.length !== 0) {
      res.status(230).json({
        status: true,
        message: "All Users retrieved",
        data: users,
      });
    } else {
      res.status(244).json({
        status: false,
        message: `Users is empty`,
      });
    }
  });
});

app.get("/users/:id", (req, res) => {
  const { id } = req.params;
  UserController.getUser(id, (error, user) => {
    if (error) {
      res.json(error);
    }

    if (user && user.id) {
      res.status(230).json({
        status: true,
        message: `User with id ${id} retrieved`,
        data: user,
      });
    } else {
      res.status(244).json({
        status: false,
        message: `User with id ${id} not found`,
      });
    }
  });
});

app.post("/users", (req, res) => {
  bcrypt.hash(req.body.password, 10, function (err, hash) {
    const user = {
      name: req.body.name,
      email: req.body.email,
      password: hash,
    };

    UserController.createUser(user, (error, userInserted) => {
      if (error) {
        res.json(error);
      }

      res.status(231).json({
        status: true,
        message: "User has been created!",
        data: userInserted,
      });
    });
  });
});

app.put("/users/:id", (req, res) => {
  const { id } = req.params;
  UserController.getUser(id, (error, user) => {
    if (error) {
      res.json(error);
    }

    if (user && user.id) {
      if (req.body.password) {
        bcrypt.hash(req.body.password, 10, (err, hash) => {
          const newUser = {
            name: req.body.name || user.name,
            email: req.body.email || user.email,
            password: hash,
          };

          UserController.updateUser(id, newUser, (error, updatedUser) => {
            if (error) {
              res.json(error);
            }

            res.status(232).json({
              status: true,
              message: `User with id ${id} updated!`,
            });
          });
        });
      } else {
        const newUser = {
          name: req.body.name || user.name,
          email: req.body.email || user.email,
        };

        UserController.updateUser(id, newUser, (error, updatedUser) => {
          if (error) {
            res.json(error);
          }

          res.status(232).json({
            status: true,
            message: `User with id ${id} updated!`,
          });
        });
      }
    }
  });
});

app.delete("/users/:id", (req, res) => {
  const { id } = req.params;
  UserController.deleteUser(id, (error, user) => {
    if (error) {
      res.json(error);
    }

    res.status(201).json({
      status: true,
      message: `User with id ${id} deleted!`,
    });
  });
});

app.get("/todos", (req, res) => {
  TodoController.getAllTodo((error, todos) => {
    if (error) {
      res.json(error);
    }

    if (todos.length !== 0) {
      res.status(230).json({
        status: true,
        message: "All todos retrieved",
        data: todos,
      });
    } else {
      res.status(244).json({
        status: false,
        message: `Todos is empty`,
      });
    }
  });
});

app.get("/todos/:id", (req, res) => {
  const { id } = req.params;
  TodoController.getTodo(id, (error, todo) => {
    if (error) {
      res.json(error);
    }

    if (todo && todo.id) {
      res.status(230).json({
        status: true,
        message: `Todo with id ${id} retrieved`,
        data: todo,
      });
    } else {
      res.status(244).json({
        status: false,
        message: `Todo with id ${id} not found`,
      });
    }
  });
});

app.post("/todos", (req, res) => {
  const userId = req.body.user_id;
  UserController.getUser(userId, (error, user) => {
    if (error) {
      res.json(error);
    }

    if (user && user.id) {
      const todo = {
        name: req.body.name,
        description: req.body.description,
        due_at: req.body.due_at,
        user_id: user.id,
      };

      TodoController.createTodo(todo, (error, todoInserted) => {
        if (error) {
          res.json(error);
        }

        res.status(231).json({
          status: true,
          message: `Todo has been added for User ${user.name}`,
          data: todo,
        });
      });
    } else {
      res.status(244).json({
        status: false,
        message: `Add todo failed due to User with id ${req.body.user_id} not found!`,
      });
    }
  });
});

app.put("/todos/:id", (req, res) => {
  const { id } = req.params;
  if (req.body.user_id) {
    const userId = req.body.user_id;
    UserController.getUser(userId, (error, user) => {
      if (error) {
        res.json(error);
      }

      if (user && user.id) {
        TodoController.getTodo(id, (error, todo) => {
          if (error) {
            res.json(error);
          }
          if (todo && todo.id) {
            const newTodo = {
              name: req.body.name || todo.name,
              description: req.body.description || todo.description,
              due_at: req.body.due_at || todo.due_at,
              user_id: req.body.user_id || todo.user_id,
            };

            TodoController.updateTodo(id, newTodo, (error, todo) => {
              if (error) {
                res.json(error);
              }

              res.status(232).json({
                status: true,
                message: `Todo with id ${req.params.id} updated and have assigned to User ${user.name}`,
              });
            });
          } else {
            res.status(244).json({
              status: false,
              message: `Todo with id ${id} not found!`,
            });
          }
        });
      } else {
        res.status(244).json({
          status: false,
          message: `Update todo failed due to User with id ${req.body.user_id} not found!`,
        });
      }
    });
  } else {
    TodoController.getTodo(id, (error, todo) => {
      if (error) {
        res.json(error);
      }
      if (todo && todo.id) {
        const newTodo = {
          name: req.body.name || todo.name,
          description: req.body.description || todo.description,
          due_at: req.body.due_at || todo.due_at,
        };

        TodoController.updateTodo(id, newTodo, (error, todo) => {
          if (error) {
            res.json(error);
          }

          res.status(232).json({
            status: true,
            message: `Todo with id ${id} updated!`,
          });
        });
      } else {
        res.status(244).json({
          status: false,
          message: `Todo with id ${id} not found!`,
        });
      }
    });
  }
});

app.delete("/todos/:id", (req, res) => {
  const { id } = req.params;
  TodoController.deleteTodo(id, (error, todo) => {
    if (error) {
      res.json(error);
    }

    if (todo) {
      res.status(233).json({
        status: true,
        message: `Todo with id ${id} deleted!`,
      });
    } else {
      res.status(244).json({
        status: false,
        message: `Todo with id ${id} not found!`,
      });
    }
  });
});

app.listen(port, () => console.log(`Listen on port ${port} using callback`));
