// const pool = require("../conn");
const { Book } = require("../models");

class BookController {
  static getAllBook(callback) {
    Book.findAll()
      .then((books) => {
        return callback(null, books);
      })
      .catch((error) => {
        return callback(error, null);
      });

    // pool.query(`select * from "Books"`, (error, result) => {
    //   if (error) {
    //     callback(error, null);
    //   } else {
    //     callback(null, result.rows);
    //   }
    // });
  }

  static getBook(id, callback) {
    Book.findByPk(id)
      .then((book) => {
        return callback(null, book);
      })
      .catch((error) => {
        return callback(error, null);
      });

    // pool.query(
    //   `select * from "Books" where id = ${id}`,
    //   (error, result) => {
    //     if (error) {
    //       callback(error, null);
    //     } else {
    //       callback(null, result.rows);
    //     }
    //   }
    // );
  }

  static createBook(book, callback) {
    Book.create(book)
      .then((book) => {
        return callback(null, book);
      })
      .catch((error) => {
        return callback(error, null);
      });

    // pool.query(
    //   `insert into "Books" (title, author, "createdAt", "updatedAt") values
    //   ('${book.title}', '${book.author}', now(), now())
    //   returning id, title, author, "createdAt", "updatedAt"`,
    //   (error, result) => {
    //     if (error) {
    //       callback(error, null);
    //     } else {
    //       callback(null, result.rows);
    //     }
    //   }
    // );
  }

  static updateBook(id, book, callback) {
    Book.update(book, { where: { id } })
      .then((updatedBook) => {
        return callback(null, updatedBook);
      })
      .catch((error) => {
        return callback(error, null);
      });
  }

  static deleteBook(id, callback) {
    Book.destroy({
      where: {
        id,
      },
    })
      .then((book) => {
        return callback(null, book);
      })
      .catch((error) => {
        return callback(error, null);
      });
  }
}

module.exports = BookController;
