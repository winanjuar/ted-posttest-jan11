const { User } = require("../models");

class UserController {
  static getAllUser(callback) {
    User.findAll({
      attributes: {
        exclude: ["password"],
      },
    })
      .then((users) => {
        return callback(null, users);
      })
      .catch((error) => {
        return callback(error, null);
      });
  }

  static getUser(id, callback) {
    User.findByPk(id)
      .then((user) => {
        return callback(null, user);
      })
      .catch((error) => {
        return callback(error, null);
      });
  }

  static createUser(user, callback) {
    User.create(user)
      .then((user) => {
        return callback(null, user);
      })
      .catch((error) => {
        return callback(error, null);
      });
  }

  static updateUser(id, user, callback) {
    User.update(user, { where: { id } })
      .then((updatedUser) => {
        return callback(null, updatedUser);
      })
      .catch((error) => {
        return callback(error, null);
      });
  }

  static deleteUser(id, callback) {
    User.destroy({
      where: {
        id,
      },
    })
      .then((user) => {
        return callback(null, user);
      })
      .catch((error) => {
        return callback(error, null);
      });
  }
}

module.exports = UserController;
