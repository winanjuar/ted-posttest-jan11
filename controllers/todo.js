const { Todo } = require("../models");

class TodoController {
  static getAllTodo(callback) {
    Todo.findAll()
      .then((todos) => {
        return callback(null, todos);
      })
      .catch((error) => {
        return callback(error, null);
      });
  }

  static getTodo(id, callback) {
    Todo.findByPk(id)
      .then((todo) => {
        return callback(null, todo);
      })
      .catch((error) => {
        return callback(error, null);
      });
  }

  static createTodo(todo, callback) {
    Todo.create(todo)
      .then((todo) => {
        return callback(null, todo);
      })
      .catch((error) => {
        return callback(error, null);
      });
  }

  static updateTodo(id, todo, callback) {
    Todo.update(todo, { where: { id } })
      .then((updatedTodo) => {
        return callback(null, updatedTodo);
      })
      .catch((error) => {
        return callback(error, null);
      });
  }

  static deleteTodo(id, callback) {
    Todo.destroy({
      where: {
        id,
      },
    })
      .then((todo) => {
        return callback(null, todo);
      })
      .catch((error) => {
        return callback(error, null);
      });
  }
}

module.exports = TodoController;
