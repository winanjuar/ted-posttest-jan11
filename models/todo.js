"use strict";
const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
  class Todo extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.User, { foreignKey: "user_id", as: "user" });
    }
  }
  Todo.init(
    {
      name: DataTypes.STRING,
      description: DataTypes.TEXT,
      due_at: DataTypes.DATE,
      user_id: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "Todo",
      tableName: "todos",
      underscored: true,
      createdAt: "created_at",
      updatedAt: "updated_at",
    }
  );

  return Todo;
};
