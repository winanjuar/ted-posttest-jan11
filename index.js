const express = require("express");
const bcrypt = require("bcrypt");

const app = express();
const port = process.env.PORT || 3000;

const { Book, User, Todo } = require("./models");

app.use(express.json());

app.get("/", (req, res) => {
  res.status(200).json({
    status: true,
    message: "Tugas TED Express Sequelize using Promise",
  });
});

app.get("/books", (req, res) => {
  Book.findAll()
    .then((books) => {
      res.status(200).json({
        status: true,
        message: "All Books retrieved",
        data: books,
      });
    })
    .catch((error) => res.json(error));
});

app.get("/books/:id", (req, res) => {
  Book.findByPk(req.params.id)
    .then((book) => {
      res.status(200).json({
        status: true,
        message: `Book with id ${req.params.id} retrieved`,
        data: book,
      });
    })
    .catch((error) => res.json(error));
});

app.post("/books", (req, res) => {
  Book.create({
    title: req.body.title,
    author: req.body.author,
  }).then((book) => {
    res.status(201).json({
      status: true,
      message: "Books created!",
      data: book,
    });
  });
});

app.put("/books/:id", (req, res) => {
  Book.findByPk(req.params.id).then((book) => {
    book
      .update(
        {
          title: req.body.title || book.title,
          author: req.body.author || book.author,
        },
        {
          where: {
            id: req.params.id,
          },
        }
      )
      .then((book) => {
        res.status(201).json({
          status: true,
          message: `Books with id ${req.params.id} updated!`,
          data: book,
        });
      });
  });
});

app.delete("/books/:id", (req, res) => {
  Book.destroy({
    where: {
      id: req.params.id,
    },
  }).then((book) => {
    res.status(201).json({
      status: true,
      message: `Books with id ${req.params.id} deleted!`,
    });
  });
});

app.get("/users", (req, res) => {
  User.findAll({
    attributes: {
      exclude: ["password"],
    },
  })
    .then((users) => {
      if (users.length !== 0) {
        res.status(230).json({
          status: true,
          message: "All Users retrieved",
          data: users,
        });
      } else {
        res.status(244).json({
          status: false,
          message: `Users is empty`,
        });
      }
    })
    .catch((error) => res.json(error));
});

app.get("/users/:id", (req, res) => {
  User.findByPk(req.params.id, {
    include: [
      {
        model: Todo,
        as: "todos",
      },
    ],
    attributes: {
      exclude: ["password"],
    },
  })
    .then((user) => {
      if (user && user.id) {
        res.status(230).json({
          status: true,
          message: `User with id ${req.params.id} retrieved`,
          data: user,
        });
      } else {
        res.status(244).json({
          status: false,
          message: `User with id ${req.params.id} not found`,
        });
      }
    })
    .catch((error) => res.json(error));
});

app.post("/users", (req, res) => {
  bcrypt.hash(req.body.password, 10, function (err, hash) {
    User.create({
      name: req.body.name,
      email: req.body.email,
      password: hash,
    })
      .then((user) => {
        res.status(231).json({
          status: true,
          message: "User has been created!",
          data: user,
        });
      })
      .catch((error) => res.json(error));
  });
});

app.put("/users/:id", (req, res) => {
  User.findByPk(req.params.id)
    .then((user) => {
      if (user && user.id) {
        if (req.body.password) {
          bcrypt.hash(req.body.password, 10, function (err, hash) {
            user
              .update(
                {
                  name: req.body.name || user.name,
                  email: req.body.email || user.email,
                  password: hash,
                },
                {
                  where: {
                    id: req.params.id,
                  },
                }
              )
              .then((user) => {
                res.status(232).json({
                  status: true,
                  message: `User with id ${req.params.id} updated!`,
                  data: user,
                });
              })
              .catch((error) => res.json(error));
          });
        } else {
          user
            .update(
              {
                name: req.body.name || user.name,
                email: req.body.email || user.email,
              },
              {
                where: {
                  id: req.params.id,
                },
              }
            )
            .then((user) => {
              res.status(232).json({
                status: true,
                message: `User with id ${req.params.id} updated!`,
                data: user,
              });
            })
            .catch((error) => res.json(error));
        }
      } else {
        res.status(244).json({
          status: false,
          message: `User with id ${req.params.id} not found!`,
        });
      }
    })
    .catch((error) => res.json(error));
});

app.delete("/users/:id", (req, res) => {
  User.destroy({
    where: {
      id: req.params.id,
    },
  })
    .then((user) => {
      if (user) {
        res.status(233).json({
          status: true,
          message: `User with id ${req.params.id} deleted!`,
        });
      } else {
        res.status(244).json({
          status: false,
          message: `User with id ${req.params.id} not found!`,
        });
      }
    })
    .catch((error) => res.json(error));
});

app.get("/todos", (req, res) => {
  Todo.findAll()
    .then((todos) => {
      if (todos.length !== 0) {
        res.status(230).json({
          status: true,
          message: "All Todos retrieved",
          data: todos,
        });
      } else {
        res.status(244).json({
          status: false,
          message: `Todos is empty`,
        });
      }
    })
    .catch((error) => res.json(error));
});

app.get("/todos/:id", (req, res) => {
  Todo.findByPk(req.params.id, {
    include: [
      {
        model: User,
        as: "user",
      },
    ],
  })
    .then((todo) => {
      if (todo && todo.id) {
        res.status(230).json({
          status: true,
          message: `Todo with id ${req.params.id} retrieved`,
          data: todo,
        });
      } else {
        res.status(244).json({
          status: false,
          message: `Todo with id ${req.params.id} not found`,
        });
      }
    })
    .catch((error) => res.json(error));
});

app.post("/todos", (req, res) => {
  User.findByPk(req.body.user_id)
    .then((user) => {
      if (user && user.id) {
        Todo.create({
          name: req.body.name,
          description: req.body.description,
          due_at: req.body.due_at,
          user_id: user.id,
        })
          .then((todo) => {
            res.status(231).json({
              status: true,
              message: `Todo has been added for User ${user.name}`,
              data: todo,
            });
          })
          .catch((error) => res.json(error));
      } else {
        res.status(244).json({
          status: false,
          message: `Add todo failed due to User with id ${req.body.user_id} not found!`,
        });
      }
    })
    .catch((error) => res.json(error));
});

app.put("/todos/:id", (req, res) => {
  if (req.body.user_id) {
    User.findByPk(req.body.user_id)
      .then((user) => {
        if (user && user.id) {
          Todo.findByPk(req.params.id)
            .then((todo) => {
              if (todo && todo.id) {
                todo
                  .update(
                    {
                      name: req.body.name || todo.name,
                      description: req.body.description || todo.description,
                      due_at: req.body.due_at || todo.due_at,
                      user_id: req.body.user_id || todo.user_id,
                    },
                    {
                      where: {
                        id: req.params.id,
                      },
                    }
                  )
                  .then((todo) => {
                    res.status(232).json({
                      status: true,
                      message: `Todo with id ${req.params.id} updated and have assigned to User ${user.name}`,
                      data: todo,
                    });
                  })
                  .catch((error) => res.json(error));
              } else {
                res.status(244).json({
                  status: false,
                  message: `Todo with id ${req.params.id} not found!`,
                });
              }
            })
            .catch((error) => res.json(error));
        } else {
          res.status(244).json({
            status: false,
            message: `Update todo failed due to User with id ${req.body.user_id} not found!`,
          });
        }
      })
      .catch((error) => {
        res.json(error);
      });
  } else {
    Todo.findByPk(req.params.id)
      .then((todo) => {
        if (todo && todo.id) {
          todo
            .update(
              {
                name: req.body.name || todo.name,
                description: req.body.description || todo.description,
                due_at: req.body.due_at || todo.due_at,
              },
              {
                where: {
                  id: req.params.id,
                },
              }
            )
            .then((todo) => {
              res.status(232).json({
                status: true,
                message: `Todo with id ${req.params.id} updated!`,
                data: todo,
              });
            })
            .catch((error) => res.json(error));
        } else {
          res.status(244).json({
            status: false,
            message: `Todo with id ${req.params.id} not found!`,
          });
        }
      })
      .catch((error) => res.json(error));
  }
});

app.delete("/todos/:id", (req, res) => {
  Todo.destroy({
    where: {
      id: req.params.id,
    },
  })
    .then((todo) => {
      if (todo) {
        res.status(233).json({
          status: true,
          message: `Todo with id ${req.params.id} deleted!`,
        });
      } else {
        res.status(244).json({
          status: false,
          message: `Todo with id ${req.params.id} not found!`,
        });
      }
    })
    .catch((error) => res.json(error));
});

app.listen(port, () => console.log(`Listen on port ${port} using promise`));
